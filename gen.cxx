// include../
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <cmath>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <png.h>
#include <unistd.h>
#include <vector>
//..
// gen../
namespace gen {
    // Data structures../
    // This data structure provides a unified way to store and access parameters for various
    // procedures, including random lines, circles, rectangles, polygons, ellipses, and more.
    // The main structure, Parameters, contains a Procedure_Type enum to identify the specific
    // procedure being used, and a union to store the corresponding parameter structure for that
    // procedure. Each procedure has its own parameter structure with fields relevactive to that
    // procedure. This approach allows for efficient memory usage and flexibility when working
    // with different procedures.
    // Procedure_Type../
    enum Procedure_Type {
        RANDOM_LINES,
        RANDOM_LINES_SEQUENTIAL,
        UNRANDOM_LINES_SEQUENTIAL,
        DERANDOMIZE_LINES,
        RANDOM_CIRCLES,
        RANDOM_RECTANGLES,
        RANDOM_POLYGONS,
        RANDOM_ELLIPSES,
        GRID_SQUARES,
        CONCENTRIC_CIRCLES,
        APOLLONIAN_GASKET,
        MANDELBROT_SET,
        BARNSLEY_FERN,
        SIERPINSKI_TRIANGLE,
        GAME_OF_LIFE,
        CYCLIC_CELL_AUTOMATON,
        RPS_WALK,
        // Add more procedures here
    };//..
    // Random_Lines../
    struct  Random_Lines{
        int width;     // Width of pixmap and x bound of lines
        int height;    // Height of pixmap and y bound of lines
        int num_lines; // Number of lines
    };//..
    // random_circles_struct ../
    struct Random_Circles {
        int width;           // Width of pixmap
        int height;          // Height of pixmap
        int num_circles;     // Number of circles
        int min_radius;      // Minimum radius of circles
        int max_radius;      // Maximum radius of circles
        bool random_colors;  // If true, use random colors for circles
    };//..
    // random_rectangles_struct ../
    struct Random_Rectangles {
        int width;           // Width of pixmap
        int height;          // Height of pixmap
        int num_rectangles;  // Number of rectangles
        int min_size;        // Minimum size of rectangles
        int max_size;        // Maximum size of rectangles
        bool random_colors;  // If true, use random colors for rectangles
    };//..
    // random_polygons_struct ../
    struct Random_Polygons {
        int width;             // Width of pixmap
        int height;            // Height of pixmap
        int num_polygons;      // Number of polygons
        int side_length;        // length of a polygon side
        int num_vertices;      // Number of vertices for each polygon
        bool random_colors;    // If true, use random colors for polygons
    };//..
    // random_ellipses_struct ../
    struct Random_Ellipses {
        int width;            // Width of pixmap
        int height;           // Height of pixmap
        int num_ellipses;     // Number of ellipses
        int min_size;         // Minimum size of ellipses
        int max_size;         // Maximum size of ellipses
        bool random_colors;   // If true, use random colors for ellipses
    };//..
    // grid_squares_struct ../
    struct Grid_Squares {
        int width;                 // Width of pixmap
        int height;                // Height of pixmap
        int num_rows;              // Number of rows in the grid
        int num_columns;           // Number of columns in the grid
        int min_size;              // Minimum size of squares
        int max_size;              // Maximum size of squares
        bool random_colors;        // If true, use random colors for squares
    };//..
    // concentric_circles_struct ../
    struct Concentric_Circles {
        int width;                // Width of pixmap
        int height;               // Height of pixmap
        int num_circles;          // Number of concentric circles
        int min_radius;           // Minimum radius for circles
        int max_radius;           // Maximum radius for circles
        bool random_colors;       // If true, use random colors for circles
    };//..
    // Apollonian_Gasket_Struct ../
    struct Apollonian_Gasket {
        int width;                // Width of pixmap
        int height;               // Height of pixmap
        int recursion_depth;      // Number of recursion levels
    };//..
    // Mandelbrot_Set_Struct ../
    struct Mandelbrot_Set {
        int width;                 // Width of pixmap
        int height;                // Height of pixmap
        double x_min;              // Minimum x value (real)
        double x_max;              // Maximum x value (real)
        double y_min;              // Minimum y value (imaginary)
        double y_max;              // Maximum y value (imaginary)
        int max_iterations;        // Maximum number of iterations
        int color_scheme;          // Color scheme for the Mandelbrot Set
    };//..
    // Barnsley_Fern_Struct ../
    struct Barnsley_Fern {
        int width;                // Width of pixmap
        int height;               // Height of pixmap
        int num_points;           // Number of points to draw
        double scale;             // Scale factor for fern size
    };//..
    // Sierpinski_Triangle_Struct ../
    struct Sierpinski_Triangle {
        int width;                // Width of pixmap
        int height;               // Height of pixmap
        int num_points;           // Number of points to draw
    };//..
    // Game_of_Life../
    struct Game_Of_Life {
        int width;
        int height;
        int cell_size;
        int num_generations;
        double live_probability;
    };//..
    // Cyclic_Cell_Automaton ../
    struct Cyclic_Cell_Automaton {
        int width;             // Width of the automaton grid
        int height;            // Height of the automaton grid
        int num_states;        // Number of states in the automaton (e.g., 3 for Rock-Paper-Scissors)
        int num_iterations;    // Number of iterations to run the automaton
        int cell_size;         // Size of each cell in pixels
    }; //..
    // RPS_Walk_Params ../
    struct RPS_Walk {
        int width;               // Width of the automaton grid
        int height;              // Height of the automaton grid
        int num_iterations;      // Number of iterations to run the automaton
        int cell_size;           // Size of each cell in pixels
    }; //..
    // Parameters../
    struct Parameters {
        Procedure_Type type;
        union {
            Random_Lines random_lines_input;
            Random_Circles random_circles_input;
            Random_Rectangles random_rectangles_input;
            Random_Polygons random_polygons_input;
            Random_Ellipses random_ellipses_input;
            Grid_Squares grid_squares_input;
            Concentric_Circles concentric_circles_input;
            Apollonian_Gasket apollonian_gasket_input;
            Mandelbrot_Set mandelbrot_set_input;
            Barnsley_Fern barnsley_fern_input;
            Sierpinski_Triangle sierpinski_triangle_input;
            Game_Of_Life game_of_life_input;
            Cyclic_Cell_Automaton cyclic_cell_automaton_input;
            RPS_Walk    rps_walk_input;
            // Add more procedure-specific parameter structs here
        };
    };//..
    //..
    //
    // Utility functions../
    // These utility functions provide a set of commonly used operations when working with
    // X11 graphics, including generating random integers, creating pixmaps, saving pixmaps
    // to files, and creating and displaying windows. These functions aim to simplify the
    // graphics-related code and make it more maintainable by abstracting away lower-level
    // details. The random_int function generates a random integer between a specified
    // minimum and maximum value, while create_pixmap and save_pixmap_to_file handle pixmap
    // creation and saving, respectively. create_and_display_window is responsible for
    // creating a window, setting its attributes, and displaying it on the screen.
    // random_int../
    int random_int(int min, int max) {
        return min + rand() % (max - min + 1);
    }//..
    // create_pixmap../
    Pixmap create_pixmap(Display *display, Window &root, int width, int height) {
        return XCreatePixmap(display, root, width, height, DefaultDepth(display, 0));
    }//..
    // save_pixmap_to_file../
    void save_pixmap_to_file(Display *display, Pixmap pixmap, int width, int height, const char *filename) {
        // Get the XImage from the Pixmap
        XImage *image = XGetImage(display, pixmap, 0, 0, width, height, AllPlanes, ZPixmap);

        // Open the file
        FILE *file = fopen(filename, "wb");
        if (!file) {
            fprintf(stderr, "Error: Unable to open file %s\n", filename);
            return;
        }

        // Initialize libpng structures
        png_structp png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
        png_infop info_ptr = png_create_info_struct(png_ptr);

        if (!png_ptr || !info_ptr) {
            fprintf(stderr, "Error: Failed to initialize libpng structures\n");
            fclose(file);
            return;
        }

        if (setjmp(png_jmpbuf(png_ptr))) {
            fprintf(stderr, "Error: Failed to write PNG file\n");
            png_destroy_write_struct(&png_ptr, &info_ptr);
            fclose(file);
            return;
        }

        // Set the output file and configure the PNG header
        png_init_io(png_ptr, file);
        png_set_IHDR(png_ptr, info_ptr, width, height,
                8, PNG_COLOR_TYPE_RGB, PNG_INTERLACE_NONE,
                PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);
        png_write_info(png_ptr, info_ptr);

        // Write the image data to the file
        for (int y = 0; y < height; y++) {
            png_bytep row = (png_bytep)image->data + y * image->bytes_per_line;
            png_write_row(png_ptr, row);
        }

        // Finish writing and clean up resources
        png_write_end(png_ptr, NULL);
        png_destroy_write_struct(&png_ptr, &info_ptr);
        fclose(file);
        XDestroyImage(image);
    }//..
    // create_and_display_window../
    Window create_and_display_window(Display *display, int screen_num, Window root, int width, int height) {
        unsigned long window_attributes_mask = CWBackPixel | CWBorderPixel;
        XSetWindowAttributes window_attributes;
        window_attributes.background_pixel = BlackPixel(display, screen_num);
        window_attributes.border_pixel = WhitePixel(display, screen_num);

        Window window = XCreateWindow(
                display,
                root,
                0,
                0,
                width,
                height,
                1,
                DefaultDepth(display, screen_num),
                InputOutput,
                CopyFromParent,
                window_attributes_mask,
                &window_attributes
                );

        XSelectInput(display, window, ExposureMask | KeyPressMask);
        XMapWindow(display, window);

        return window;
    }//..
    // red_to_green../
unsigned int red_to_green(unsigned int red_color) {
    // Extract the red component from the input color
    unsigned int red_component = (red_color >> 16) & 0xFF;

    // Create the green color by scaling the red intensity to green
    unsigned int green_color = (red_component << 8) & 0xFF00;

    // Combine the green color with the lower 16 bits of the input color (blue and alpha components)
    return (green_color | (red_color & 0xFFFF));
}//..
// red_to_blue../
unsigned int red_to_blue(unsigned int red_color) {
    // Extract the red component from the input color
    unsigned int red_component = (red_color >> 16) & 0xFF;

    // Create the blue color by scaling the red intensity to blue
    unsigned int blue_color = red_component & 0xFF;

    // Combine the blue color with the upper 16 bits of the input color (green and alpha components)
    return ((red_color & 0xFFFF00) | blue_color);
}//..
//..
    //
    // Image Generators../
    // random_lines../
    void random_lines(
            Display             *display, // The display that holds the pixmap
            Pixmap              pixmap,   // The pixmap that holds the image
            GC                  gc,       // Graphics Context to draw line
            Parameters          params    // width and height and # of lines
            ) {
        // Set the foreground color to white and clear the background
        XSetForeground(display, gc, WhitePixel(display, DefaultScreen(display)));
        XFillRectangle(display, pixmap, gc, 0, 0, params.random_lines_input.width, params.random_lines_input.height);

        // Set the foreground color to black for drawing lines
        XSetForeground(display, gc, BlackPixel(display, DefaultScreen(display)));

        // Draw random lines
        for (int i = 0; i < params.random_lines_input.num_lines; ++i) {
            int x1 = random_int(0, params.random_lines_input.width);
            int y1 = random_int(0, params.random_lines_input.height);
            int x2 = random_int(0, params.random_lines_input.width);
            int y2 = random_int(0, params.random_lines_input.height);

            XDrawLine(display, pixmap, gc, x1, y1, x2, y2);
        }
    }//..
    // random_lines_sequential../
    void random_lines_sequential(
            Display             *display, // The display that holds the pixmap
            Pixmap              pixmap,   // The pixmap that holds the image
            GC                  gc,       // Graphics Context to draw line
            Parameters          params    // width and height and # of lines
            ) {
        // Set the foreground color to white and clear the background
        XSetForeground(display, gc, WhitePixel(display, DefaultScreen(display)));
        XFillRectangle(display, pixmap, gc, 0, 0, params.random_lines_input.width, params.random_lines_input.height);

        // Draw random lines connecting sequential random points
        int prev_x = random_int(0, params.random_lines_input.width);
        int prev_y = random_int(0, params.random_lines_input.height);

        int num_colors = 3; // Number of color transitions (yellow->red, red->green, green->blue)

        for (int i = 0; i < params.random_lines_input.num_lines; ++i) {
            int x = random_int(0, params.random_lines_input.width);
            int y = random_int(0, params.random_lines_input.height);

            // Generate a color based on the order in which the lines are drawn
            int segment = (i * num_colors) / params.random_lines_input.num_lines;
            float t = (i * num_colors) % params.random_lines_input.num_lines / static_cast<float>(params.random_lines_input.num_lines);
            int r, g, b;

            if (segment == 0) {
                // Yellow to red
                r = 255;
                g = 255 * (1 - t);
                b = 0;
            } else if (segment == 1) {
                // Red to green
                r = 255 * (1 - t);
                g = 255 * t;
                b = 0;
            } else {
                // Green to blue
                r = 0;
                g = 255 * (1 - t);
                b = 255 * t;
            }

            unsigned long color = (r << 16) | (g << 8) | b;

            XSetForeground(display, gc, color);

            XDrawLine(display, pixmap, gc, prev_x, prev_y, x, y);
            prev_x = x;
            prev_y = y;
        }
    }//..
    // unrandom_lines_sequential../
    void unrandom_lines_sequential(
            Display             *display, // The display that holds the pixmap
            Pixmap              pixmap,   // The pixmap that holds the image
            GC                  gc,       // Graphics Context to draw line
            Parameters          params    // width and height and # of lines
            ) {
        // Set the foreground color to white and clear the background
        XSetForeground(display, gc, WhitePixel(display, DefaultScreen(display)));
        XFillRectangle(display, pixmap, gc, 0, 0, params.random_lines_input.width, params.random_lines_input.height);

        // Draw lines connecting sequential points generated by a deterministic pattern
        int prev_x = params.random_lines_input.width / 2;
        int prev_y = params.random_lines_input.height / 2;

        for (int i = 0; i < params.random_lines_input.num_lines; ++i) {
            int x = (prev_x + i * 10) % params.random_lines_input.width;
            int y = (prev_y + i * 20) % params.random_lines_input.height;

            // Generate a color based on the order in which the lines are drawn
            float t = static_cast<float>(i) / params.random_lines_input.num_lines;
            int r = 255 * t;
            int g = 255 * (1 - t);
            int b = 255 * (1 - t);

            unsigned long color = (r << 16) | (g << 8) | b;
            XSetForeground(display, gc, color);

            XDrawLine(display, pixmap, gc, prev_x, prev_y, x, y);
            prev_x = x;
            prev_y = y;
        }
    }//..
    // random_circles ../
    void random_circles(
            Display             *display, // The display that holds the pixmap
            Pixmap              pixmap,   // The pixmap that holds the image
            GC                  gc,       // Graphics Context to draw circles
            Parameters          params    // Width, height, and number of circles
            ) {
        // Clear the background
        XSetForeground(display, gc, WhitePixel(display, DefaultScreen(display)));
        XFillRectangle(display, pixmap, gc, 0, 0, params.random_circles_input.width, params.random_circles_input.height);

        // Draw random circles
        for (int i = 0; i < params.random_circles_input.num_circles; ++i) {
            int x = random_int(0, params.random_circles_input.width);
            int y = random_int(0, params.random_circles_input.height);
            int radius = random_int(params.random_circles_input.min_radius, params.random_circles_input.max_radius);

            if (params.random_circles_input.random_colors) {
                unsigned long color = random_int(0, (1 << 24) - 1);
                XSetForeground(display, gc, color);
            } else {
                XSetForeground(display, gc, BlackPixel(display, DefaultScreen(display)));
            }

            XDrawArc(display, pixmap, gc, x - radius, y - radius, 2 * radius, 2 * radius, 0, 360 * 64);
        }
    }//..
    // random_rectangles ../
    void random_rectangles(
            Display             *display, // The display that holds the pixmap
            Pixmap              pixmap,   // The pixmap that holds the image
            GC                  gc,       // Graphics Context to draw rectangles
            Parameters          params    // Width, height, and number of rectangles
            ) {
        // Clear the background
        XSetForeground(display, gc, WhitePixel(display, DefaultScreen(display)));
        XFillRectangle(display, pixmap, gc, 0, 0, params.random_rectangles_input.width, params.random_rectangles_input.height);

        // Draw random rectangles
        for (int i = 0; i < params.random_rectangles_input.num_rectangles; ++i) {
            int x = random_int(0, params.random_rectangles_input.width);
            int y = random_int(0, params.random_rectangles_input.height);
            int w = random_int(params.random_rectangles_input.min_size, params.random_rectangles_input.max_size);
            int h = random_int(params.random_rectangles_input.min_size, params.random_rectangles_input.max_size);

            if (params.random_rectangles_input.random_colors) {
                unsigned long color = random_int(0, (1 << 24) - 1);
                XSetForeground(display, gc, color);
            } else {
                XSetForeground(display, gc, BlackPixel(display, DefaultScreen(display)));
            }

            XDrawRectangle(display, pixmap, gc, x, y, w, h);
        }
    }//..
    // random_polygons ../
    // This code contains two functions:
    // 1. generate_polygon_points: Computes the coordinates of a regular polygon's
    // vertices given its center, number of vertices, and side length.
    // 2. random_polygons: Draws a specified number of random polygons on a pixmap
    // with optional random colors, given display, pixmap, graphics context, and polygon parameters.
    void generate_polygon_points(XPoint *points, int num_vertices, int side_length, int x, int y) {
        double angle_increment = (2 * M_PI) / num_vertices;

        points[0].x = x;
        points[0].y = y;

        for (int i = 1; i < num_vertices; ++i) {
            double angle = i * angle_increment;
            points[i].x = x + side_length * cos(angle);
            points[i].y = y + side_length * sin(angle);
        }
    }
    void random_polygons(
            Display             *display,
            Pixmap              pixmap,
            GC                  gc,
            Parameters          params
            ) {
        // Clear the background
        XSetForeground(display, gc, WhitePixel(display, DefaultScreen(display)));
        XFillRectangle(display, pixmap, gc, 0, 0, params.random_polygons_input.width, params.random_polygons_input.height);

        // Draw random polygons
        for (int i = 0; i < params.random_polygons_input.num_polygons; ++i) {
            XPoint points[params.random_polygons_input.num_vertices];

            int x = random_int(0, params.random_polygons_input.width);
            int y = random_int(0, params.random_polygons_input.height);

            generate_polygon_points(points, params.random_polygons_input.num_vertices, params.random_polygons_input.side_length, x, y);

            if (params.random_polygons_input.random_colors) {
                unsigned long color = random_int(0, (1 << 24) - 1);
                XSetForeground(display, gc, color);
            } else {
                XSetForeground(display, gc, BlackPixel(display, DefaultScreen(display)));
            }

            XDrawLines(display, pixmap, gc, points, params.random_polygons_input.num_vertices, CoordModeOrigin);
            XDrawLine(display, pixmap, gc, points[params.random_polygons_input.num_vertices - 1].x, points[params.random_polygons_input.num_vertices - 1].y, points[0].x, points[0].y);
        }
    }//..
    // random_ellipses ../
    void random_ellipses(
            Display             *display, // The display that holds the pixmap
            Pixmap              pixmap,   // The pixmap that holds the image
            GC                  gc,       // Graphics Context to draw ellipses
            Parameters          params    // Width, height, and number of ellipses
            ) {
        // Clear the background
        XSetForeground(display, gc, WhitePixel(display, DefaultScreen(display)));
        XFillRectangle(display, pixmap, gc, 0, 0, params.random_ellipses_input.width, params.random_ellipses_input.height);

        // Draw random ellipses
        for (int i = 0; i < params.random_ellipses_input.num_ellipses; ++i) {
            int x = random_int(0, params.random_ellipses_input.width);
            int y = random_int(0, params.random_ellipses_input.height);
            int w = random_int(params.random_ellipses_input.min_size, params.random_ellipses_input.max_size);
            int h = random_int(params.random_ellipses_input.min_size, params.random_ellipses_input.max_size);

            if (params.random_ellipses_input.random_colors) {
                unsigned long color = random_int(0, (1 << 24) - 1);
                XSetForeground(display, gc, color);
            } else {
                XSetForeground(display, gc, BlackPixel(display, DefaultScreen(display)));
            }

            XDrawArc(display, pixmap, gc, x, y, w, h, 0, 360 * 64);
        }
    }//..
    // grid_squares ../
    // This code contains the grid_squares function, which:
    // 1. Clears the background of a pixmap with a white color.
    // 2. Calculates the grid cell size based on the input width, height, and grid specifications.
    // 3. Iterates through each grid cell, drawing a square centered within the cell.
    // 4. Optionally uses random colors for the squares, otherwise uses black color.
    void grid_squares(
            Display             *display, // The display that holds the pixmap
            Pixmap              pixmap,   // The pixmap that holds the image
            GC                  gc,       // Graphics Context to draw squares
            Parameters          params    // Width, height, and grid specifications
            ) {
        // Clear the background
        XSetForeground(display, gc, WhitePixel(display, DefaultScreen(display)));
        XFillRectangle(display, pixmap, gc, 0, 0, params.grid_squares_input.width, params.grid_squares_input.height);

        // Calculate grid cell size
        int cell_width = params.grid_squares_input.width / params.grid_squares_input.num_columns;
        int cell_height = params.grid_squares_input.height / params.grid_squares_input.num_rows;

        // Draw grid of squares
        for (int row = 0; row < params.grid_squares_input.num_rows; ++row) {
            for (int col = 0; col < params.grid_squares_input.num_columns; ++col) {
                int x = col * cell_width;
                int y = row * cell_height;
                int size = random_int(params.grid_squares_input.min_size, params.grid_squares_input.max_size);

                if (params.grid_squares_input.random_colors) {
                    unsigned long color = random_int(0, (1 << 24) - 1);
                    XSetForeground(display, gc, color);
                } else {
                    XSetForeground(display, gc, BlackPixel(display, DefaultScreen(display)));
                }

                // Center the square within the grid cell
                int x_offset = (cell_width - size) / 2;
                int y_offset = (cell_height - size) / 2;
                XDrawRectangle(display, pixmap, gc, x + x_offset, y + y_offset, size, size);
            }
        }
    }//..
    // concentric_circles ../
    void concentric_circles(
            Display             *display, // The display that holds the pixmap
            Pixmap              pixmap,   // The pixmap that holds the image
            GC                  gc,       // Graphics Context to draw circles
            Parameters          params    // Width, height, and number of circles
            ) {
        // Clear the background
        XSetForeground(display, gc, WhitePixel(display, DefaultScreen(display)));
        XFillRectangle(display, pixmap, gc, 0, 0, params.concentric_circles_input.width, params.concentric_circles_input.height);

        // Calculate center of circles
        int center_x = params.concentric_circles_input.width / 2;
        int center_y = params.concentric_circles_input.height / 2;

        // Draw concentric circles
        for (int i = 0; i < params.concentric_circles_input.num_circles; ++i) {
            int radius = random_int(params.concentric_circles_input.min_radius, params.concentric_circles_input.max_radius);
            int diameter = radius * 2;

            if (params.concentric_circles_input.random_colors) {
                unsigned long color = random_int(0, (1 << 24) - 1);
                XSetForeground(display, gc, color);
            } else {
                XSetForeground(display, gc, BlackPixel(display, DefaultScreen(display)));
            }

            XDrawArc(display, pixmap, gc, center_x - radius, center_y - radius, diameter, diameter, 0, 360 * 64);
        }
    }//..
    // apollonian_gasket ../
    // This code contains two functions to draw an Apollonian Gasket:
    // 1. draw_circle_recursive: A recursive function that draws circles and their
    //    corresponding smaller circles at a specified depth, up to a maximum depth.
    // 2. apollonian_gasket: Sets up the initial drawing environment (clears
    //    background, sets foreground color) and calls draw_circle_recursive with
    //    the appropriate initial parameters.
    void draw_circle_recursive(Display *display, Pixmap pixmap, GC gc, int x, int y, int radius, int depth, int max_depth) {
        if (depth >= max_depth) {
            return;
        }

        XDrawArc(display, pixmap, gc, x - radius, y - radius, radius * 2, radius * 2, 0, 360 * 64);

        int new_radius = radius / 2;
        draw_circle_recursive(display, pixmap, gc, x - new_radius, y, new_radius, depth + 1, max_depth);
        draw_circle_recursive(display, pixmap, gc, x + new_radius, y, new_radius, depth + 1, max_depth);
        draw_circle_recursive(display, pixmap, gc, x, y - new_radius, new_radius, depth + 1, max_depth);
        draw_circle_recursive(display, pixmap, gc, x, y + new_radius, new_radius, depth + 1, max_depth);
    }
    void apollonian_gasket(
            Display             *display, // The display that holds the pixmap
            Pixmap              pixmap,   // The pixmap that holds the image
            GC                  gc,       // Graphics Context to draw circles
            Parameters          params    // Width, height, and recursion depth
            ) {
        // Clear the background
        XSetForeground(display, gc, WhitePixel(display, DefaultScreen(display)));
        XFillRectangle(display, pixmap, gc, 0, 0, params.apollonian_gasket_input.width, params.apollonian_gasket_input.height);

        // Set the foreground color to black for drawing circles
        XSetForeground(display, gc, BlackPixel(display, DefaultScreen(display)));

        int initial_radius = std::min(params.apollonian_gasket_input.width, params.apollonian_gasket_input.height) / 2;
        int center_x = params.apollonian_gasket_input.width / 2;
        int center_y = params.apollonian_gasket_input.height / 2;

        draw_circle_recursive(display, pixmap, gc, center_x, center_y, initial_radius, 0, params.apollonian_gasket_input.recursion_depth);
    }//..
    // mandelbrot_set ../
    // The Mandelbrot Set is a mathematical object that represents a set of complex
    // numbers for which a specific function (z = z^2 + c) does not diverge when
    // iterated from z = 0. The set is typically visualized by coloring the points
    // in the complex plane according to their escape time. The points that belong
    // to the Mandelbrot Set are usually colored black, while points outside the
    // set are assigned different colors based on the number of iterations it takes
    // for them to escape the set (i.e., their escape time).
    //
    // Complex numbers are numbers that have both a real and an imaginary part,
    // usually written in the form a + bi, where a and b are real numbers, and i is
    // the imaginary unit (i^2 = -1). The complex plane is a two-dimensional plane
    // in which complex numbers can be represented as points, with the real part
    // corresponding to the horizontal axis and the imaginary part to the vertical
    // axis.
    //
    // The term "diverge" in the context of the Mandelbrot Set refers to the
    // behavior of the function (z = z^2 + c) when iterated repeatedly. If the
    // magnitude (i.e., absolute value) of the resulting complex number becomes
    // infinitely large after a certain number of iterations, the function is said
    // to diverge for that specific complex number (c). Conversely, if the
    // magnitude remains bounded after infinite iterations, the function is said to
    // converge for that complex number, and the number is considered to be part of
    // the Mandelbrot Set.
    //
    // "Escape time" is a term used to describe the number of iterations it takes
    // for a complex number to diverge.
    //
    //
    // Example 1 (Diverges): Let's take the complex number c = 1 + i. When we
    // iterate the function (z = z^2 + c) starting from z = 0, we get the following
    // sequence of complex numbers:
    // 0^2 + (1 + i) = 1 + i
    // (1 + i)^2 + (1 + i) = 1 + 3i
    // (1 + 3i)^2 + (1 + i) = -7 + 7i
    // ...
    // The magnitude of the complex numbers in the sequence grows rapidly and
    // becomes unbounded, so the function diverges for c = 1 + i. Thus, this complex
    // number is not part of the Mandelbrot Set.
    //
    // Example 2 (Converges): Now, let's take the complex number c = -0.5 + 0.5i. When we
    // iterate the function (z = z^2 + c) starting from z = 0, we get the following
    // sequence of complex numbers:
    //
    // 0^2 + (-0.5 + 0.5i) = -0.5 + 0.5i
    // (-0.5 + 0.5i)^2 + (-0.5 + 0.5i) = -0.5
    // (-0.5)^2 + (-0.5 + 0.5i) = -0.25 + 0.5i = -0.6875 + 0.25i
    // ...
    // In this case, the sequence of complex numbers converges and remains bounded in magnitude:
    //
    // Example 3 (Complex Number Calculation)
    //
    // Calculate z^2 + c for z = (-0.5 + 0.5i) and c = (-0.5 + 0.5i)
    //
    // Step 1: Square z the complex number (-0.5 + 0.5i)^2.
    // To do this, use the distributive property
    // (-0.5 + 0.5i) * (-0.5 + 0.5i) = (-0.5 * -0.5) + 2(-0.5 * 0.5i) + (0.5i)^2  <-- i^2 = -1
    // = 0.25 - 0.5i - 0.25
    // = -0.5i
    //
    // Step 2: Add the other complex number (-0.5 + 0.5i) to the result from Step 1:
    // (-0.5i) + (-0.5 + 0.5i) = -0.5i + (-0.5 + 0.5i)
    // = -0.5
    //
    // So the correct calculation is:
    // (-0.5 + 0.5i)^2 + (-0.5 + 0.5i) = -0.5
    //
    unsigned long get_color(int iterations, int max_iterations, int color_scheme) {
        if (iterations == max_iterations) {
            return 0; // Black for points inside the Mandelbrot Set
        }

        // You can implement various color schemes here, for now, we'll use a simple grayscale color scheme
        double ratio = static_cast<double>(iterations) / max_iterations;
        int value = static_cast<int>(ratio * 255);

        return (value << 16) | (value << 8) | value;
    }
    int mandelbrot_escape_time(double real, double imag, int max_iterations) {
        double z_real = 0.0;
        double z_imag = 0.0;
        int iterations = 0;

        while (iterations < max_iterations && (z_real * z_real + z_imag * z_imag) < 4.0) {
            double z_real_temp = z_real * z_real - z_imag * z_imag + real;
            z_imag = 2.0 * z_real * z_imag + imag;
            z_real = z_real_temp;
            iterations++;
        }

        return iterations;
    }
    void mandelbrot_set(
            Display             *display, // The display that holds the pixmap
            Pixmap              pixmap,   // The pixmap that holds the image
            GC                  gc,       // Graphics Context to draw pixels
            Parameters          params    // Width, height, range and color scheme
            ) {
        double x_range = params.mandelbrot_set_input.x_max - params.mandelbrot_set_input.x_min;
        double y_range = params.mandelbrot_set_input.y_max - params.mandelbrot_set_input.y_min;

        for (int y = 0; y < params.mandelbrot_set_input.height; ++y) {
            for (int x = 0; x < params.mandelbrot_set_input.width; ++x) {
                double real = params.mandelbrot_set_input.x_min + x * x_range / params.mandelbrot_set_input.width;
                double imag = params.mandelbrot_set_input.y_min + y * y_range / params.mandelbrot_set_input.height;

                int iterations = mandelbrot_escape_time(real, imag, params.mandelbrot_set_input.max_iterations);

                // Set color based on color scheme and iterations
                unsigned long color = get_color(iterations, params.mandelbrot_set_input.max_iterations, params.mandelbrot_set_input.color_scheme);
                XSetForeground(display, gc, color);
                XDrawPoint(display, pixmap, gc, x, y);
            }
        }
    }//..
    // barnsley_fern ../
    // The Barnsley Fern algorithm generates a fern-like fractal pattern using a set
    // of affine transformations. It uses an iterative function system (IFS) and
    // applies one of the four possible transformations to a point, chosen randomly
    // based on predefined probabilities.

    // Transformation 1 (1% probability): Represents the stem of the fern
    // x' = 0.0
    // y' = 0.16 * y

    // Transformation 2 (85% probability): Represents the successively smaller leaflets
    // x' = 0.85 * x + 0.04 * y
    // y' = -0.04 * x + 0.85 * y + 1.6

    // Transformation 3 (7% probability): Represents the largest left-hand leaflet
    // x' = 0.2 * x - 0.26 * y
    // y' = 0.23 * x + 0.22 * y + 1.6

    // Transformation 4 (7% probability): Represents the largest right-hand leaflet
    // x' = -0.15 * x + 0.28 * y
    // y' = 0.26 * x + 0.24 * y + 0.44

    // The fern shape emerges because each transformation contributes to a specific
    // part of the overall pattern. The probabilities associated with each
    // transformation reflect the relative frequencies of each fern component in
    // nature. As the algorithm iterates through a large number of points, it
    // transforms and scales them according to these transformations, eventually
    // generating the distinctive fern-like pattern.
    void barnsley_fern(
            Display             *display, // The display that holds the pixmap
            Pixmap              pixmap,   // The pixmap that holds the image
            GC                  gc,       // Graphics Context to draw points
            Parameters          params    // Width, height, number of points, and scale
            ) {
        // Clear the background
        XSetForeground(display, gc, WhitePixel(display, DefaultScreen(display)));
        XFillRectangle(display, pixmap, gc, 0, 0, params.barnsley_fern_input.width, params.barnsley_fern_input.height);

        // Set the foreground color to black for drawing points
        XSetForeground(display, gc, BlackPixel(display, DefaultScreen(display)));

        double x = 0.0;
        double y = 0.0;

        for (int i = 0; i < params.barnsley_fern_input.num_points; ++i) {
            double random_value = static_cast<double>(rand()) / RAND_MAX;

            double new_x, new_y;
            if (random_value < 0.01) {
                new_x = 0.0;
                new_y = 0.16 * y;
            } else if (random_value < 0.86) {
                new_x = 0.85 * x + 0.04 * y;
                new_y = -0.04 * x + 0.85 * y + 1.6;
            } else if (random_value < 0.93) {
                new_x = 0.2 * x - 0.26 * y;
                new_y = 0.23 * x + 0.22 * y + 1.6;
            } else {
                new_x = -0.15 * x + 0.28 * y;
                new_y = 0.26 * x + 0.24 * y + 0.44;
            }

            x = new_x;
            y = new_y;

            int screen_x = static_cast<int>(params.barnsley_fern_input.width / 2 + x * params.barnsley_fern_input.scale);
            int screen_y = static_cast<int>(params.barnsley_fern_input.height - y * params.barnsley_fern_input.scale);

            XDrawPoint(display, pixmap, gc, screen_x, screen_y);
        }
    }//..
    // sierpinski_triangle ../
    // This function generates the Sierpinski Triangle fractal pattern by using a
    // simple random process known as the "chaos game." The Sierpinski Triangle
    // consists of three equally spaced vertices that form an equilateral triangle.

    // 1. Initialize an array of vertices representing the three corners of the
    //    equilateral triangle.
    // 2. Set the initial point (x, y) to the center of the triangle.
    // 3. For each iteration (number of points specified in 'params'):
    //    a. Choose a random vertex from the triangle.
    //    b. Move the current point halfway towards the chosen vertex.
    //    c. Draw the new point on the pixmap.
    // 4. As the number of iterations increases, the points will gradually form the
    //    distinctive pattern of the Sierpinski Triangle.

    // Note that the algorithm does not directly compute the triangle's structure.
    // Instead, it generates the pattern through a series of random steps, with the
    // final image emerging as the number of points increases.
    //
    // The Sierpinski Triangle's shape emerges from the chaos game algorithm due to
    // the self-similar, recursive nature of the fractal. The process of moving
    // the current point halfway towards a randomly chosen vertex effectively
    // subdivides the equilateral triangle into four smaller equilateral triangles.
    // The drawn point falls into one of the three smaller triangles surrounding the
    // central triangle, which remains empty. This process is repeated, and with
    // each iteration, the pattern becomes more refined, recursively creating
    // triangles within triangles.

    // The key to understanding why the shape emerges lies in the fact that the
    // probability of reaching any particular point within the triangle is
    // determined by its position relative to the vertices. Since the algorithm
    // always moves the point halfway towards a vertex, certain areas within the
    // triangle are more likely to be visited than others. Over many iterations,
    // this biased probability distribution results in the emergence of the
    // Sierpinski Triangle pattern.

    // In summary, the Sierpinski Triangle pattern emerges from the combination of
    // a simple geometric rule (moving halfway towards a vertex) and the
    // probabilities associated with reaching various points within the triangle.
    // This creates a recursive, self-similar pattern that is characteristic of
    // fractals like the Sierpinski Triangle.
    void sierpinski_triangle(
            Display             *display, // The display that holds the pixmap
            Pixmap              pixmap,   // The pixmap that holds the image
            GC                  gc,       // Graphics Context to draw points
            Parameters          params    // Width, height, and number of points
            ) {
        // Clear the background
        XSetForeground(display, gc, WhitePixel(display, DefaultScreen(display)));
        XFillRectangle(display, pixmap, gc, 0, 0, params.sierpinski_triangle_input.width, params.sierpinski_triangle_input.height);

        // Set the foreground color to black for drawing points
        XSetForeground(display, gc, BlackPixel(display, DefaultScreen(display)));

        double vertices[3][2] = {
            {params.sierpinski_triangle_input.width / 2.0, 0},
            {0, (double)params.sierpinski_triangle_input.height},
            {(double)params.sierpinski_triangle_input.width, (double)params.sierpinski_triangle_input.height}
        };

        double x = static_cast<double>(params.sierpinski_triangle_input.width) / 2;
        double y = static_cast<double>(params.sierpinski_triangle_input.height) / 2;

        for (int i = 0; i < params.sierpinski_triangle_input.num_points; ++i) {
            int vertex = rand() % 3;
            x = (x + vertices[vertex][0]) / 2;
            y = (y + vertices[vertex][1]) / 2;

            int screen_x = static_cast<int>(x);
            int screen_y = static_cast<int>(y);

            XDrawPoint(display, pixmap, gc, screen_x, screen_y);
        }
    }//..
    // game_of_life../
    // This implementation of Conway's Game of Life simulates a cellular automaton
    // on a toroidal grid (i.e., a grid with wrap-around edges). The Game of Life
    // consists of an initial configuration of "live" and "dead" cells on a grid,
    // and the evolution of the grid according to a set of rules.
    //
    // The function takes four arguments:
    // 1. display: The display that holds the pixmap.
    // 2. pixmap: The pixmap that holds the image.
    // 3. gc: Graphics Context to draw points and rectangles.
    // 4. params: A structure containing input parameters: the width and
    //    height of the grid, the size of each cell, the initial probability of
    //    a cell being alive, and the number of generations to simulate.
    //
    // The simulation proceeds as follows:
    // 1. The grid is initialized with a random distribution of live cells based on
    //    the provided probability.
    // 2. For each generation, the new state of each cell is calculated according
    //    to the following rules:
    //    a. A live cell with two or three live neighbors stays alive.
    //    b. A dead cell with exactly three live neighbors becomes alive.
    //    c. In all other cases, the cell dies or remains dead.
    // 3. The new state of the grid is swapped with the old state, and the process
    //    is repeated for the specified number of generations.
    // 4. The final state of the grid is drawn on the pixmap using the provided
    //    Graphics Context.
    //
    // Conway's Game of Life exhibits a variety of interesting behaviors, including
    // oscillators, gliders, and other complex structures, depending on the initial
    // configuration of the grid. The toroidal grid ensures that patterns are not
    // confined to the boundaries of the grid, allowing for more diverse and
    // long-lived configurations to emerge.
    //
    void game_of_life(Display *display, Pixmap pixmap, GC gc, Parameters params) {
        int num_rows = params.game_of_life_input.height / params.game_of_life_input.cell_size;
        int num_cols = params.game_of_life_input.width / params.game_of_life_input.cell_size;

        std::vector<std::vector<bool>> grid(num_rows, std::vector<bool>(num_cols, false));
        std::vector<std::vector<bool>> new_grid(num_rows, std::vector<bool>(num_cols, false));

        XSetForeground(display, gc, WhitePixel(display, DefaultScreen(display)));
        XFillRectangle(display, pixmap, gc, 0, 0, params.sierpinski_triangle_input.width, params.sierpinski_triangle_input.height);

        // Initialize the grid with random live cells
        for (int row = 0; row < num_rows; ++row) {
            for (int col = 0; col < num_cols; ++col) {
                grid[row][col] = (rand() / (double)RAND_MAX) < params.game_of_life_input.live_probability;
            }
        }

        // Run the Game of Life for the specified number of generations
        for (int generation = 0; generation < params.game_of_life_input.num_generations; ++generation) {
            for (int row = 0; row < num_rows; ++row) {
                for (int col = 0; col < num_cols; ++col) {
                    int live_neighbors = 0;

                    for (int dr = -1; dr <= 1; ++dr) {
                        for (int dc = -1; dc <= 1; ++dc) {
                            if (dr == 0 && dc == 0) continue;

                            int neighbor_row = (row + dr + num_rows) % num_rows;
                            int neighbor_col = (col + dc + num_cols) % num_cols;

                            if (grid[neighbor_row][neighbor_col]) {
                                ++live_neighbors;
                            }
                        }
                    }

                    if (grid[row][col]) {
                        new_grid[row][col] = live_neighbors == 2 || live_neighbors == 3;
                    } else {
                        new_grid[row][col] = live_neighbors == 3;
                    }
                }
            }

            grid.swap(new_grid);
        }

        // Draw the final state
        XSetForeground(display, gc, BlackPixel(display, DefaultScreen(display)));
        for (int row = 0; row < num_rows; ++row) {
            for (int col = 0; col < num_cols; ++col) {
                if (grid[row][col]) {
                    XFillRectangle(display, pixmap, gc, col * params.game_of_life_input.cell_size, row * params.game_of_life_input.cell_size,
                            params.game_of_life_input.cell_size, params.game_of_life_input.cell_size);
                }
            }
        }
    }//..
    // cyclic_cell_automaton ../
    // This implementation of a Cyclic Cellular Automaton simulates a grid of cells,
    // where each cell can be in one of a given number of states. The grid evolves
    // based on a simple set of rules, and the automaton is known for its ability to
    // generate complex patterns from simple initial conditions.
    //
    // The function takes four arguments:
    // 1. display: The display that holds the pixmap.
    // 2. pixmap: The pixmap that holds the image.
    // 3. gc: Graphics Context to draw cells.
    // 4. params: A structure containing input parameters, such as the width and
    //    height of the grid, the number of states, the number of iterations, and
    //    the size of each cell.
    //
    // The simulation proceeds as follows:
    // 1. The grid is initialized with random states for each cell.
    // 2. For each iteration, the new state of each cell is calculated according
    //    to the following rules:
    //    a. If a cell has a neighbor (including diagonals) in the next state (i.e.,
    //       (current_state + 1) % num_states), the cell advances to the next state.
    //    b. Otherwise, the cell remains in its current state.
    // 3. The new state of the grid is swapped with the old state, and the process
    //    is repeated for the specified number of iterations.
    // 4. The final state of the grid is drawn on the pixmap using the provided
    //    Graphics Context, with each state represented by a different color.
    //
    // Cyclic Cellular Automata can generate a wide range of patterns, including
    // spirals, waves, and other complex structures. The simple rules governing the
    // automaton's behavior allow for emergent complexity and provide an interesting
    // example of how simple interactions can lead to intricate and diverse outcomes.
    void cyclic_cell_automaton(
            Display             *display, // The display that holds the pixmap
            Pixmap              pixmap,   // The pixmap that holds the image
            GC                  gc,       // Graphics Context to draw cells
            Parameters          params    // Automaton parameters
            ) {

        int cell_size       = params.cyclic_cell_automaton_input.cell_size;
        int width           = params.cyclic_cell_automaton_input.width/cell_size;
        int height          = params.cyclic_cell_automaton_input.height/cell_size;
        int num_states      = params.cyclic_cell_automaton_input.num_states;
        int num_iterations  = params.cyclic_cell_automaton_input.num_iterations;

        std::vector<std::vector<int>> grid(height, std::vector<int>(width));
        std::vector<std::vector<int>> new_grid(height, std::vector<int>(width));

        // Initialize the grid with random states
        for (int i = 0; i < height; ++i) {
            for (int j = 0; j < width; ++j) {
                grid[i][j] = random_int(0, num_states - 1);
            }
        }

        // Run the automaton for the specified number of iterations
        for (int iter = 0; iter < num_iterations; ++iter) {
            for (int i = 0; i < height; ++i) {
                for (int j = 0; j < width; ++j) {
                    int current_state = grid[i][j];
                    int next_state = (current_state + 1) % num_states;

                    bool has_neighbor = false;
                    for (int x_offset = -1; x_offset <= 1; ++x_offset) {
                        for (int y_offset = -1; y_offset <= 1; ++y_offset) {
                            if (x_offset == 0 && y_offset == 0) continue;

                            int neighbor_x = (j + x_offset + width) % width;
                            int neighbor_y = (i + y_offset + height) % height;

                            if (grid[neighbor_y][neighbor_x] == next_state) {
                                has_neighbor = true;
                                break;
                            }
                        }
                        if (has_neighbor) break;
                    }

                    new_grid[i][j] = has_neighbor ? next_state : current_state;
                }
            }
            grid.swap(new_grid);
        }

        // Draw the automaton grid
        for (int i = 0; i < height; ++i) {
            for (int j = 0; j < width; ++j) {
                int state = grid[i][j];
                unsigned long color = (state + 1) * (1 << 24) / (num_states + 1);
                color = red_to_blue(color);
                color = red_to_green(color);
                XSetForeground(display, gc, color);
                XFillRectangle(display, pixmap, gc, j * cell_size, i * cell_size, cell_size, cell_size);
            }
        }
    } //..
    // rps_walk../
// rps_walk is a function that simulates a unique Rock-Paper-Scissors cellular automata
// on a grid with an "active" cell that can move, turn, and create new cells. The function updates the grid
// based on specific rules for each type of cell (Rock, Paper, or Scissors) and the active cell's interactions.
//
// On each iteration, the following steps are performed:
// 1. The grid is updated using the iterate_grid function, which updates the state of each cell based on the
//    Rock-Paper-Scissors automata rules and the states of their adjacent cells.
// 2. The active cell follows these rules:
//    a. If the active cell is ROCK, it turns right.
//    b. If the active cell is PAPER, it attempts to move in its current direction, but cannot move onto a NULL square.
//    c. If the active cell is SCISSORS, it creates a new cell in its current direction at the first NULL cell encountered.
//
// The grid is initialized with random Rock, Paper, or Scissors cells in the 16 center cells and iterated for a given
// number of iterations. After the iterations, the final grid state is displayed using XFillRectangle.
//
    struct Adjacent_Cells {
        int north;
        int east;
        int west;
        int south;
    };

    // The subprocedure to return the updated adjacent cells
    Adjacent_Cells get_adjacent_cells(const std::vector<std::vector<int>>& grid, int x, int y, int width, int height) {
        Adjacent_Cells adjacent_cells;

        // Find the adjacent cells
        adjacent_cells.north = (y > 0) ? grid[y - 1][x] : 0;
        adjacent_cells.south = (y < height - 1) ? grid[y + 1][x] : 0;
        adjacent_cells.west = (x > 0) ? grid[y][x - 1] : 0;
        adjacent_cells.east = (x < width - 1) ? grid[y][x + 1] : 0;

        // Update the adjacent cells based on the conditions
        if (adjacent_cells.north == 0 && adjacent_cells.south == 0) {
            adjacent_cells.north = 0;
            adjacent_cells.south = 0;
        } else if (adjacent_cells.north == 0) {
            int temp_y = y + 1;
            while (temp_y < height && grid[temp_y][x] != 0) {
                temp_y++;
            }
            adjacent_cells.north = (temp_y < 0) ? grid[temp_y - 1][x] : 0;
        } else if (adjacent_cells.south == 0) {
            int temp_y = y - 1;
            while (temp_y >= 0 && grid[temp_y][x] != 0) {
                temp_y--;
            }
            adjacent_cells.south = (temp_y >= 0) ? grid[temp_y + 1][x] : 0;
        }

        if (adjacent_cells.east == 0 && adjacent_cells.west == 0) {
            adjacent_cells.east = 0;
            adjacent_cells.west = 0;
        } else if (adjacent_cells.east == 0) {
            int temp_x = x - 1;
            while (temp_x < width && grid[y][temp_x] != 0) {
                temp_x--;
            }
            adjacent_cells.east = (temp_x >= 0) ? grid[y][temp_x + 1] : 0;
        } else if (adjacent_cells.west == 0) {
            int temp_x = x + 1;
            while (temp_x >= 0 && grid[y][temp_x] < width) {
                temp_x++;
            }
            adjacent_cells.west = (temp_x < width) ? grid[y][temp_x - 1] : 0;
        }

        return adjacent_cells;
    }
    // rps_solve
    int rps_solve(int input, int mech)
    {
        int tmp, tmp2, tmp3;

        tmp = input ^ mech;
        tmp3 = (tmp >> 1) | (tmp << 1) | tmp;
        tmp2 = tmp ^ tmp3;
        tmp2 = tmp2 ^ tmp3;
        tmp = ~tmp3 & input;
        return (tmp ^ tmp2) & 3;
    }
    void iterate_grid(std::vector<std::vector<int>>& old_grid, int width, int height) {
        // Create a new grid with the same dimensions as the old grid
        std::vector<std::vector<int>> new_grid(height, std::vector<int>(width, 0));

        // Iterate over each cell in the old grid
        for (int y = 0; y < height; ++y) {
            for (int x = 0; x < width; ++x) {
                if(old_grid[y][x] == 0) {
                    new_grid[y][x] = 0;
                    continue;
                }
                // Get the updated adjacent cells for the current cell
                Adjacent_Cells adjacent_cells = get_adjacent_cells(old_grid, x, y, width, height);

                // Solve the cells together using rps_solve()
                int sol1 = rps_solve(adjacent_cells.north, adjacent_cells.south);
                int sol2 = rps_solve(adjacent_cells.east, adjacent_cells.west);

                // Update the new grid with the solved value
                new_grid[y][x] = rps_solve(sol1, sol2);
            }
        }

        // Set the old grid equal to the new grid
        old_grid = new_grid;
    }
    // rps_walk
    void rps_walk(Display *display, Pixmap pixmap, GC gc, Parameters p) {
        const int NULL_STATE = 0;
        const int ROCK = 1;
        const int PAPER = 2;
        const int SCISSORS = 3;

        int cell_size = p.rps_walk_input.cell_size;
        int width = p.rps_walk_input.width/cell_size;
        int height = p.rps_walk_input.height/cell_size;
        int iterations = p.rps_walk_input.num_iterations;

        std::vector<std::vector<int>> grid(height, std::vector<int>(width, NULL_STATE));

        // Initialize the 16 center cells with random Rock, Paper, or Scissors
        for (int i = -2; i <= 2; ++i) {
            for (int j = -2; j <= 2; ++j) {
                grid[height / 2 + i][width / 2 + j] = random_int(1, 3);
            }
        }

        int active_x = width / 2;
        int active_y = height / 2;
        int active_direction = 0;

        for (int i = 0; i < iterations; i++) {
            iterate_grid(grid, width, height);
            // Ant's move
            int current_cell = grid[active_y][active_x];
            int dx = (active_direction == 1 ? 1 : (active_direction == 3 ? -1 : 0));
            int dy = (active_direction == 0 ? -1 : (active_direction == 2 ? 1 : 0));

            int next_x = active_x + dx;
            int next_y = active_y + dy;

            if (current_cell == ROCK)
                // Action 1: Turn direction right
                active_direction = (active_direction + 1) % 4;
            else if (next_x >= 0 && next_x < width && next_y >= 0 && next_y < height) {
                if (current_cell == PAPER) {
                    // Action 2: Attempt to walk in the direction it is facing (it cannot walk onto a NULL square)
                    if (grid[next_y][next_x] != NULL_STATE) {
                        active_x = next_x;
                        active_y = next_y;
                    }
                } else if (current_cell == SCISSORS) {
                    // Action 3: Create a new square in the direction it's facing
                    while ((next_x >= 0 && next_x < width && next_y >= 0 && next_y < height) &&
                            (grid[next_y][next_x] != 0) ) {
                        next_x += dx;
                        next_y += dy;
                    }
                    if (next_x >= 0 && next_x < width && next_y >= 0 && next_y < height)
                        grid[next_y][next_x] = grid[next_y - dy][next_x - dx];
                }
            }
        }
        // Display the final result
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                int cell_value = grid[y][x];
                unsigned long color;
                switch (cell_value) {
                    case ROCK:
                        color = 0x6699CC; // Blue-green-gray (ROCK)
                        break;
                    case PAPER:
                        color = 0x99CCCC; // Lighter blue-green-gray (PAPER)
                        break;
                    case SCISSORS:
                        color = 0x336699; // Darker blue-green-gray (SCISSORS)
                        break;
                    default:
                        color = 0x000000; // Black (NULL_STATE)
                        break;
                }
                XSetForeground(display, gc, color);
                XFillRectangle(display, pixmap, gc, x * cell_size, y * cell_size, cell_size, cell_size);
            }
        }
    }//..
    // call_procedure../
    void call_procedure(Display *display, Pixmap pixmap, GC gc, Parameters params) {
        switch (params.type) {
            case RANDOM_LINES:
                random_lines(display, pixmap, gc, params);
                break;
            case RANDOM_LINES_SEQUENTIAL: // Add this case
                random_lines_sequential(display, pixmap, gc, params);
                break;
            case UNRANDOM_LINES_SEQUENTIAL: // Add this case
                unrandom_lines_sequential(display, pixmap, gc, params);
                break;
            case DERANDOMIZE_LINES:
                random_lines(display, pixmap, gc, params);
                break;
            case RANDOM_CIRCLES:
                random_circles(display, pixmap, gc, params);
                break;
            case RANDOM_RECTANGLES:
                random_rectangles(display, pixmap, gc, params);
                break;
            case RANDOM_POLYGONS:
                random_polygons(display, pixmap, gc, params);
                break;
            case RANDOM_ELLIPSES:
                random_ellipses(display, pixmap, gc, params);
                break;
            case GRID_SQUARES:
                grid_squares(display, pixmap, gc, params);
                break;
            case CONCENTRIC_CIRCLES:
                concentric_circles(display, pixmap, gc, params);
                break;
            case APOLLONIAN_GASKET:
                apollonian_gasket(display, pixmap, gc, params);
                break;
            case MANDELBROT_SET:
                mandelbrot_set(display, pixmap, gc, params);
                break;
            case BARNSLEY_FERN:
                barnsley_fern(display, pixmap, gc, params);
                break;
            case SIERPINSKI_TRIANGLE:
                sierpinski_triangle(display, pixmap, gc, params);
                break;
            case GAME_OF_LIFE:
                game_of_life(display, pixmap, gc, params);
                break;
            case CYCLIC_CELL_AUTOMATON:
                cyclic_cell_automaton(display, pixmap, gc, params);
                break;
            case RPS_WALK:
                rps_walk(display, pixmap, gc, params);
                break;
                // Add more procedure cases here
        }
    }//..
    //..
}//..
// Main function../
int main() {

    srand(time(nullptr));
    // Initialize the X11 library and create a pixmap
    Display *display = XOpenDisplay(NULL);
    if (display == NULL) {
        std::cerr << "Cannot open display\n";
        exit(1);
    }

    int screen_num = DefaultScreen(display);
    Window root = RootWindow(display, screen_num);

    // Define a vector of Parameters for each procedure
    std::vector<gen::Parameters> params_list;

    // Define Inputs for the various generators../
    gen::Parameters params_random_lines;
    params_random_lines.type = gen::RANDOM_LINES;
    params_random_lines.random_lines_input.width = 800;
    params_random_lines.random_lines_input.height = 600;
    params_random_lines.random_lines_input.num_lines = 100;
    params_list.push_back(params_random_lines);

    gen::Parameters params_random_lines_sequential;
    params_random_lines_sequential.type = gen::RANDOM_LINES_SEQUENTIAL;
    params_random_lines_sequential.random_lines_input.width = 800;
    params_random_lines_sequential.random_lines_input.height = 600;
    params_random_lines_sequential.random_lines_input.num_lines = 100;
    params_list.push_back(params_random_lines_sequential);

    gen::Parameters params_unrandom_lines_sequential;
    params_unrandom_lines_sequential.type = gen::UNRANDOM_LINES_SEQUENTIAL;
    params_unrandom_lines_sequential.random_lines_input.width = 800;
    params_unrandom_lines_sequential.random_lines_input.height = 600;
    params_unrandom_lines_sequential.random_lines_input.num_lines = 100;
    params_list.push_back(params_unrandom_lines_sequential);

    // Set the parameters for random circles
    gen::Parameters random_circles_params;
    random_circles_params.type = gen::RANDOM_CIRCLES;
    random_circles_params.random_circles_input.width = 800;
    random_circles_params.random_circles_input.height = 600;
    random_circles_params.random_circles_input.num_circles = 20;
    random_circles_params.random_circles_input.min_radius = 5;
    random_circles_params.random_circles_input.max_radius = 50;
    random_circles_params.random_circles_input.random_colors = true;
    params_list.push_back(random_circles_params);

    // Set the parameters for random rectangles
    gen::Parameters random_rectangles_params;
    random_rectangles_params.type = gen::RANDOM_RECTANGLES;
    random_rectangles_params.random_rectangles_input.width = 800;
    random_rectangles_params.random_rectangles_input.height = 600;
    random_rectangles_params.random_rectangles_input.num_rectangles = 20;
    random_rectangles_params.random_rectangles_input.min_size = 5;
    random_rectangles_params.random_rectangles_input.max_size = 50;
    random_rectangles_params.random_rectangles_input.random_colors = true;
    params_list.push_back(random_rectangles_params);
    //
    // Set the parameters for random ellipses
    gen::Parameters random_ellipses_params;
    random_ellipses_params.type = gen::RANDOM_ELLIPSES;
    random_ellipses_params.random_ellipses_input.width = 800;
    random_ellipses_params.random_ellipses_input.height = 600;
    random_ellipses_params.random_ellipses_input.num_ellipses = 30;
    random_ellipses_params.random_ellipses_input.min_size = 20;
    random_ellipses_params.random_ellipses_input.max_size = 100;
    random_ellipses_params.random_ellipses_input.random_colors = true;
    params_list.push_back(random_ellipses_params);

    // Set the parameters for grid of squares
    gen::Parameters grid_squares_params;
    grid_squares_params.type = gen::GRID_SQUARES;
    grid_squares_params.grid_squares_input.width = 800;
    grid_squares_params.grid_squares_input.height = 600;
    grid_squares_params.grid_squares_input.num_rows = 5;
    grid_squares_params.grid_squares_input.num_columns = 5;
    grid_squares_params.grid_squares_input.min_size = 20;
    grid_squares_params.grid_squares_input.max_size = 100;
    grid_squares_params.grid_squares_input.random_colors = true;
    params_list.push_back(grid_squares_params);

    // Set the parameters for concentric circles
    gen::Parameters concentric_circles_params;
    concentric_circles_params.type = gen::CONCENTRIC_CIRCLES;
    concentric_circles_params.concentric_circles_input.width = 800;
    concentric_circles_params.concentric_circles_input.height = 600;
    concentric_circles_params.concentric_circles_input.num_circles = 10;
    concentric_circles_params.concentric_circles_input.min_radius = 20;
    concentric_circles_params.concentric_circles_input.max_radius = 100;
    concentric_circles_params.concentric_circles_input.random_colors = true;
    params_list.push_back(concentric_circles_params);

    // Set the parameters for random polygons
    gen::Parameters random_polygons_params;
    random_polygons_params.type = gen::RANDOM_POLYGONS;
    random_polygons_params.random_polygons_input.width = 800;
    random_polygons_params.random_polygons_input.height = 600;
    random_polygons_params.random_polygons_input.num_polygons = 20;
    random_polygons_params.random_polygons_input.num_vertices = 15;
    random_polygons_params.random_polygons_input.side_length = 50;
    random_polygons_params.random_polygons_input.random_colors = true;
    params_list.push_back(random_polygons_params);



    // Set the parameters for Apollonian Gasket
    gen::Parameters apollonian_gasket_params;
    apollonian_gasket_params.type = gen::APOLLONIAN_GASKET;
    apollonian_gasket_params.apollonian_gasket_input.width = 800;
    apollonian_gasket_params.apollonian_gasket_input.height = 600;
    apollonian_gasket_params.apollonian_gasket_input.recursion_depth = 5;
    params_list.push_back(apollonian_gasket_params);

    // Set the parameters for Barnsley Fern
    gen::Parameters barnsley_fern_params;
    barnsley_fern_params.type = gen::BARNSLEY_FERN;
    barnsley_fern_params.barnsley_fern_input.width = 800;
    barnsley_fern_params.barnsley_fern_input.height = 600;
    barnsley_fern_params.barnsley_fern_input.num_points = 50000;
    barnsley_fern_params.barnsley_fern_input.scale = 40;
    params_list.push_back(barnsley_fern_params);

    // Set the parameters for Mandelbrot Set
    gen::Parameters mandelbrot_params;
    mandelbrot_params.type = gen::MANDELBROT_SET;
    mandelbrot_params.mandelbrot_set_input.width = 800;
    mandelbrot_params.mandelbrot_set_input.height = 600;
    mandelbrot_params.mandelbrot_set_input.x_min = -1.25;
    mandelbrot_params.mandelbrot_set_input.x_max = -1.15;
    mandelbrot_params.mandelbrot_set_input.y_min = -0.25;
    mandelbrot_params.mandelbrot_set_input.y_max = -0.15;
    mandelbrot_params.mandelbrot_set_input.max_iterations = 500;
    mandelbrot_params.mandelbrot_set_input.color_scheme = 0;
    params_list.push_back(mandelbrot_params);

    gen::Parameters sierpinski_triangle_params;
    sierpinski_triangle_params.type = gen::SIERPINSKI_TRIANGLE;
    sierpinski_triangle_params.sierpinski_triangle_input.width = 800;
    sierpinski_triangle_params.sierpinski_triangle_input.height = 600;
    sierpinski_triangle_params.sierpinski_triangle_input.num_points = 15000;
    params_list.push_back(sierpinski_triangle_params);

    gen::Parameters game_of_life_params;
    game_of_life_params.type = gen::GAME_OF_LIFE;
    game_of_life_params.game_of_life_input.width = 800;
    game_of_life_params.game_of_life_input.height = 600;
    game_of_life_params.game_of_life_input.cell_size = 3;
    game_of_life_params.game_of_life_input.num_generations = 100;
    game_of_life_params.game_of_life_input.live_probability = .4;
    params_list.push_back(game_of_life_params);

    gen::Parameters cyclic_cell_automaton_params;
    cyclic_cell_automaton_params.type = gen::CYCLIC_CELL_AUTOMATON;
    cyclic_cell_automaton_params.cyclic_cell_automaton_input.width = 800;
    cyclic_cell_automaton_params.cyclic_cell_automaton_input.height = 600;
    cyclic_cell_automaton_params.cyclic_cell_automaton_input.num_states = 7;
    cyclic_cell_automaton_params.cyclic_cell_automaton_input.num_iterations = 500;
    cyclic_cell_automaton_params.cyclic_cell_automaton_input.cell_size = 7;
    params_list.push_back(cyclic_cell_automaton_params);

    gen::Parameters rps_walk_params;
    rps_walk_params.type = gen::RPS_WALK;
    rps_walk_params.rps_walk_input.width = 800;
    rps_walk_params.rps_walk_input.height = 600;
    rps_walk_params.rps_walk_input.num_iterations = 3500;
    rps_walk_params.rps_walk_input.cell_size = 7;
    params_list.push_back(rps_walk_params);
    //..

    // Create a pixmap for each set of parameters
    std::vector<Pixmap> pixmaps;
    for (const gen::Parameters &params : params_list) {
        Pixmap pixmap = gen::create_pixmap(display, root, params.random_lines_input.width, params.random_lines_input.height);
        pixmaps.push_back(pixmap);
    }

    // Set up a graphics context
    GC gc = XCreateGC(display, root, 0, NULL);

    // Call the procedures for each set of parameters and corresponding pixmap
    for (size_t i = 0; i < params_list.size(); ++i) {
        gen::call_procedure(display, pixmaps[i], gc, params_list[i]);
    }

    // Create and display a window
    Window window = gen::create_and_display_window(display, screen_num, root, 800, 600);

    // Display pixmap and handle key events
    XEvent event;
    bool running = true;
    size_t current_pixmap_index = 0;
    while (running) {
        XNextEvent(display, &event);
        if (event.type == Expose) {
            XCopyArea(
                    display,
                    pixmaps[current_pixmap_index],
                    window,
                    gc,
                    0,
                    0,
                    params_list[current_pixmap_index].random_lines_input.width,
                    params_list[current_pixmap_index].random_lines_input.height,
                    0,
                    0
                    );
        } else if (event.type == KeyPress) {
            // Handle left and right arrow keys to switch between pixmaps
            int keysym = XLookupKeysym(&event.xkey, 0);
            if (keysym == XK_Left) {
                if (current_pixmap_index == 0) {
                    current_pixmap_index = pixmaps.size() - 1;
                } else {
                    --current_pixmap_index;
                }
            } else if (keysym == XK_Right) {
                current_pixmap_index = (current_pixmap_index + 1) % pixmaps.size();
            } else {
                running = false;
            }
            XEvent expose_event;
            expose_event.type = Expose;
            expose_event.xexpose.window = window;
            XSendEvent(display, window, False, ExposureMask, &expose_event);
        }
    }
    // Clean up resources and exit
    XFreeGC(display, gc);
    for (Pixmap &pixmap : pixmaps)
        XFreePixmap(display, pixmap);
    XCloseDisplay(display);

    return 0;
}//..
