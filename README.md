# C++ Module Repository

This repository contains a collection of reusable C++ modules designed for use in various programming projects. These modules are created as a temporary solution until the Jai programming language is available. Each module is defined within a namespace and can be easily integrated into your projects by copy-pasting them into your source code.

## Table of Contents

1. [Getting Started](#getting-started)
2. [Available Modules](#available-modules)
3. [Coding Conventions and Best Practices](#coding-conventions-and-best-practices)
4. [Contributing](#contributing)
5. [Versioning](#versioning)
6. [License](#license)

## Getting Started

To use a module from this repository, simply copy the desired module's namespace from the corresponding source file and paste it at the top of your project file. Use the folding feature of your text editor to hide the module's code and prevent it from cluttering your workspace.

## Available Modules

- viz: Tools for X11 visualization
- 

For detailed information on each module, including their purpose, functionality, and usage, refer to the individual module's documentation within the source code.

## Coding Conventions and Best Practices

When creating and modifying modules, adhere to the following coding conventions and best practices:

- Use `Snake_Case` for struct definitions.
- Use `snake_case` for variables and procedures.

## Contributing

We welcome contributions to this repository. To contribute, please follow these steps:

1. Fork the repository.
2. Create a new branch for your feature or bugfix.
3. Implement your changes, adhering to the coding conventions and best practices established in the [Coding Conventions and Best Practices](#coding-conventions-and-best-practices) section.
4. Test your changes thoroughly.
5. Update or create documentation as needed.
6. Submit a pull request to merge your changes into the main branch.

## Versioning

This repository uses [semantic versioning](https://semver.org/) to manage module releases. Each module's version number consists of three parts: major, minor, and patch.

- Major version: Indicates breaking changes.
- Minor version: Introduces new features without breaking existing functionality.
- Patch version: Provides bug fixes and minor improvements.

## License

Copyright (C) 2023 by Crystal Core Systems

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.


Feel free to use, modify, and distribute these modules according to the terms of the specified license.
