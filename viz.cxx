// libraries... ../
#include <iostream>
#include <vector>
#include <utility>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/extensions/Xrender.h>
#include <X11/keysym.h>
#include <tuple>
#include <cstdlib>
#include <ctime>
#include <cmath>
#include <algorithm> // for demo
//..

#define block namespace

// viz module ../
//  -- (requires X11 and Xrender)
// This module provides functionality for creating, initializing, and managing X11 windows and
// their content on logical screens, enabling efficient window management and interaction.
//
// Usage:
// 1. Define a Window_Config struct, specifying logical screens, windows, and pixmap dimensions.
// 2. Pass the Window_Config to create_windows, which returns a populated vector of Screen_Info structs.
// 3. Each Screen_Info struct represents a logical screen containing multiple subwindows.
// 4. Each subwindow is associated with a pixmap, which holds the content to be displayed.
// 5. The window acts as a viewport onto the pixmap, showing only a subsection of the pixmap content.
//    The logical window is used to map a section of the pixmap to the window.
// 6. Use handle_scrolling to scroll the content of a window by modifying the position of the logical window on the pixmap.
// 7. Use zoom to change the zoom level of the content by resizing the logical window and updating the logical window's position on the pixmap.
// 8. Use redraw_pixmap to render the content of the pixmap onto the window, taking into account the position and dimensions of the logical window.
//
// The module is designed to be used with X11 windowing systems and offers a streamlined approach to creating,
// managing, and interacting with windows and their content. Users can easily manage windows and their content
// using logical screens, window positions, and pixmaps.

block viz { 
    // structs ../
    struct Window_Config {
        std::vector<std::tuple<int, int, int, int>> logical_screens; // x, y, width, height
        std::vector<std::vector<std::tuple<int, int, int, int>>> window_positions_per_screen; // x, y, width, height
        std::vector<std::vector<std::tuple<int, int>>> pixmap_dimensions_per_screen;
        std::vector<int> active_window; // The window in the screen to make active
        int active_screen; // The screen to make active when the program initializes
    };

    struct Window_Info {
        Window window;
        GC gc;
        Pixmap pixmap;
        // The window as displayed on the screen
        int x_position;
        int y_position;
        int width;
        int height;
        // sizes for rescaling
        int max_width;
        int min_width;
        int max_height;
        int min_height;
        // A window implemented in logic which translates the pixmap to the actual window
        // this allows for scrolling and zoom
        int logical_x;
        int logical_y;
        int logical_width;
        int logical_height;
        // The entire content available to the window
        int pixmap_width;
        int pixmap_height;
        int border_width  = 0;
        int border_height = 0;
    };

    struct Screen_Info {
        Display* display;
        Window logical_screen;
        int screen_number;
        int x_position;
        int y_position;
        int width;
        int height;
        int active_window;
        std::vector<Window_Info> windows;
    };
    //..
    // create_x11_display ../
    //    --  initializes X11 display connections for a vector of Screen_Info structs.
    void create_x11_display(std::vector<Screen_Info>& screen_info_list, int active_screen) {
        Display* display = XOpenDisplay(NULL);
        if (display == nullptr) {
            std::cerr << "Cannot open display" << std::endl;
            exit(1);
        }

        int screen_number = DefaultScreen(display);
        for (size_t i = 0; i < screen_info_list.size(); ++i) {
            if (i == active_screen) continue;

            auto& screen_info = screen_info_list[i];
            Window root_window = DefaultRootWindow(display);

            // Create a new window for the screen_info
            Window logical_screen = XCreateSimpleWindow(display, root_window,
                    screen_info.x_position, screen_info.y_position,
                    screen_info.width, screen_info.height,
                    0, // Border width
                    BlackPixel(display, screen_number),
                    WhitePixel(display, screen_number));

            // Set the window properties and map it
            XSelectInput(display, logical_screen,
                    ExposureMask | KeyPressMask | FocusChangeMask);
            XMapWindow(display, logical_screen);

            for (auto& window_info : screen_info.windows) {
                window_info.window = XCreateSimpleWindow(
                        display, 
                        logical_screen, // Use logical_screen as the parent
                        window_info.x_position, window_info.y_position,
                        window_info.width, window_info.height, 
                        0, // border width
                        BlackPixel(display, screen_number),
                        WhitePixel(display, screen_number));

                // Create a pixmap for the window
                window_info.pixmap = XCreatePixmap(display, logical_screen, 
                        window_info.pixmap_width, window_info.pixmap_height, 
                        DefaultDepth(display, screen_number));

                XMapWindow(display, window_info.window);
                window_info.gc = XCreateGC(display, window_info.window, 0, NULL);
            }

            // Set the fields in screen_info
            screen_info.display = display;
            screen_info.logical_screen = logical_screen;
            screen_info.screen_number = screen_number;
        }
        auto& screen_info = screen_info_list[active_screen];
        Window root_window = DefaultRootWindow(display);

        // Create a new window for the screen_info
        Window logical_screen = XCreateSimpleWindow(display, root_window,
                screen_info.x_position, screen_info.y_position,
                screen_info.width, screen_info.height,
                0, // Border width
                BlackPixel(display, screen_number),
                WhitePixel(display, screen_number));

        // Set the window properties and map it
        XSelectInput(display, logical_screen,
                ExposureMask | KeyPressMask | FocusChangeMask);
        XMapWindow(display, logical_screen);

        for (auto& window_info : screen_info.windows) {
            window_info.window = XCreateSimpleWindow(
                    display, 
                    logical_screen, // Use logical_screen as the parent
                    window_info.x_position, window_info.y_position,
                    window_info.width, window_info.height, 
                    0, // border width
                    BlackPixel(display, screen_number),
                    WhitePixel(display, screen_number));

            // Create a pixmap for the window
            window_info.pixmap = XCreatePixmap(display, logical_screen, 
                    window_info.pixmap_width, window_info.pixmap_height, 
                    DefaultDepth(display, screen_number));

            XMapWindow(display, window_info.window);
            window_info.gc = XCreateGC(display, window_info.window, 0, NULL);
        }

        // Set the fields in screen_info
        screen_info.display = display;
        screen_info.logical_screen = logical_screen;
        screen_info.screen_number = screen_number;
    } //..
    // setup_windows ../
    //    -- configures windows and their positions for each screen.
    // 1. Loop through each Screen_Info in screen_info_list.
    // 2. For each screen, loop through window_positions_per_screen.
    // 3. Create and populate a Window_Info struct with position, dimensions, and pixmap size.
    // 4. Add the Window_Info struct to the corresponding Screen_Info's windows vector.

    void setup_windows(
            std::vector<Screen_Info>& screen_info_list,
            const std::vector<std::vector<std::tuple<int, int, int, int>>>& window_positions_per_screen,
            const std::vector<std::vector<std::tuple<int, int>>>& pixmap_dimensions_per_screen) 
    {

        for (size_t i = 0; i < screen_info_list.size(); ++i) {
            for (size_t j = 0; j < window_positions_per_screen[i].size(); ++j) {
                const auto& window_position = window_positions_per_screen[i][j];
                Window_Info window_info;
                window_info.x_position = std::get<0>(window_position);
                window_info.y_position = std::get<1>(window_position);
                window_info.logical_x = 0;
                window_info.logical_y = 0;
                window_info.width = std::get<2>(window_position);
                window_info.height = std::get<3>(window_position);
                window_info.logical_width = window_info.width;
                window_info.logical_height = window_info.height;

                // Initialize pixmap width and height
                const auto& pixmap_size = pixmap_dimensions_per_screen[i][j];
                window_info.pixmap_width = std::get<0>(pixmap_size);
                window_info.pixmap_height = std::get<1>(pixmap_size);

                screen_info_list[i].windows.push_back(window_info);
            }
        }
    }//..
    // create_windows ../
    //    -- configures and initializes windows according to the given Window_Config.
    // 1. Prepare the screen_info_list vector based on the provided logical_screens.
    // 2. Create an X11 display for each Screen_Info.
    // 3. Set up windows using window_positions_per_screen and pixmap_dimensions_per_screen.
    // 4. Initialize and render the windows.
    // 5. Return the populated screen_info_list vector.

    std::vector<Screen_Info> create_windows(const Window_Config& config) {
        // Prepare screen_info_list
        std::vector<Screen_Info> screen_info_list(config.logical_screens.size());

        // Set up windows with pixmap sizes
        setup_windows(screen_info_list, config.window_positions_per_screen, config.pixmap_dimensions_per_screen);

        for (size_t i = 0; i < config.logical_screens.size(); ++i) {
            screen_info_list[i].x_position = std::get<0>(config.logical_screens[i]);
            screen_info_list[i].y_position = std::get<1>(config.logical_screens[i]);
            screen_info_list[i].width = std::get<2>(config.logical_screens[i]);
            screen_info_list[i].height = std::get<3>(config.logical_screens[i]);
            screen_info_list[i].active_window = config.active_window[i];
        }
        // Create X11 display
        create_x11_display(screen_info_list, config.active_screen);
        return screen_info_list;
    }//..
    // draw_pixmap ../
    //    -- Copies and scales the relevant portion of a pixmap to fit the window:
    // 1. Retrieve the depth of the root window.
    // 2. Create a logical pixmap with dimensions matching the logical window size.
    // 3. Copy the relevant portion of the original pixmap to the logical pixmap.
    // 4. Set up XRender to scale the logical pixmap to the window dimensions.
    // 5. Apply the scaling transform and composite the source and destination pictures.
    // 6. Free the XRender picture resources and the logical pixmap.

    void draw_pixmap(const Screen_Info& screen_info, int window_index) {

        Window_Info window_info = screen_info.windows[window_index];

        Pixmap pixmap = window_info.pixmap;
        Display* display = screen_info.display;

        // Get the depth of the root window
        XWindowAttributes root_attr;
        XGetWindowAttributes(display, screen_info.logical_screen, &root_attr);
        int depth = root_attr.depth;

        // Create a logical pixmap with dimensions of logical_width and logical_height
        Pixmap logical_pixmap = XCreatePixmap(display, window_info.window, window_info.logical_width, window_info.logical_height, depth);

        // Copy the relevant portion of the original pixmap to the logical pixmap
        XCopyArea(display, pixmap, logical_pixmap, window_info.gc, window_info.logical_x, window_info.logical_y,
                window_info.logical_width, window_info.logical_height, 0, 0);

        XRenderPictFormat* pict_format = XRenderFindStandardFormat(display, PictStandardRGB24);
        Picture source_picture = XRenderCreatePicture(display, logical_pixmap, pict_format, 0, NULL);
        Picture dest_picture = XRenderCreatePicture(display, window_info.window, pict_format, 0, NULL);

        double scale_x = static_cast<double>(window_info.logical_width) / static_cast<double>(window_info.width);
        double scale_y = static_cast<double>(window_info.logical_height) / static_cast<double>(window_info.height);

        XTransform xform = {{
            {XDoubleToFixed(scale_x), XDoubleToFixed(0), XDoubleToFixed(0)},
                {XDoubleToFixed(0), XDoubleToFixed(scale_y), XDoubleToFixed(0)},
                {XDoubleToFixed(0), XDoubleToFixed(0), XDoubleToFixed(1)}
        }};

        XRenderSetPictureTransform(display, source_picture, &xform);
        XRenderComposite(display, PictOpSrc, source_picture, None, dest_picture,
                0, 0, 0, 0, 0, 0, window_info.width, window_info.height);

        XRenderFreePicture(display, source_picture);
        XRenderFreePicture(display, dest_picture);
        XFreePixmap(display, logical_pixmap);
    }//..
    // redraw_pixmap ../
    //    -- Copies and scales the relevant portion of a pixmap to fit the window:
    // 1. Retrieve the depth of the root window.
    // 2. Create a logical pixmap with dimensions matching the logical window size.
    // 3. Copy the relevant portion of the original pixmap to the logical pixmap.
    // 4. Set up XRender to scale the logical pixmap to the window dimensions.
    // 5. Apply the scaling transform and composite the source and destination pictures.
    // 6. Free the XRender picture resources and the logical pixmap.

    void redraw_pixmap(const Screen_Info& screen_info) {

        int window_index = screen_info.active_window;
        Window_Info window_info = screen_info.windows[window_index];

        Pixmap pixmap = window_info.pixmap;
        Display* display = screen_info.display;

        // Get the depth of the root window
        XWindowAttributes root_attr;
        XGetWindowAttributes(display, screen_info.logical_screen, &root_attr);
        int depth = root_attr.depth;

        // Create a logical pixmap with dimensions of logical_width and logical_height
        Pixmap logical_pixmap = XCreatePixmap(display, window_info.window, window_info.logical_width, window_info.logical_height, depth);

        // Copy the relevant portion of the original pixmap to the logical pixmap
        XCopyArea(display, pixmap, logical_pixmap, window_info.gc, window_info.logical_x, window_info.logical_y,
                window_info.logical_width, window_info.logical_height, 0, 0);

        XRenderPictFormat* pict_format = XRenderFindStandardFormat(display, PictStandardRGB24);
        Picture source_picture = XRenderCreatePicture(display, logical_pixmap, pict_format, 0, NULL);
        Picture dest_picture = XRenderCreatePicture(display, window_info.window, pict_format, 0, NULL);

        double scale_x = static_cast<double>(window_info.logical_width) / static_cast<double>(window_info.width);
        double scale_y = static_cast<double>(window_info.logical_height) / static_cast<double>(window_info.height);

        XTransform xform = {{
            {XDoubleToFixed(scale_x), XDoubleToFixed(0), XDoubleToFixed(0)},
                {XDoubleToFixed(0), XDoubleToFixed(scale_y), XDoubleToFixed(0)},
                {XDoubleToFixed(0), XDoubleToFixed(0), XDoubleToFixed(1)}
        }};

        XRenderSetPictureTransform(display, source_picture, &xform);
        XRenderComposite(display, PictOpSrc, source_picture, None, dest_picture,
                0, 0, 0, 0, 0, 0, window_info.width, window_info.height);

        XRenderFreePicture(display, source_picture);
        XRenderFreePicture(display, dest_picture);
        XFreePixmap(display, logical_pixmap);
    }//..
    // scroll_window ../
    //    -- Scrolls the window based on the specified direction (dx, dy) and step:
    // 1. Calculate the scaling factor based on the ratio of the logical window size to the pixmap size.
    // 2. Update the scroll_step based on the scaling factor to ensure smooth scrolling.
    // 3. Calculate the new logical x and y positions with bounds checking.
    // 4. Update the logical x and y positions while ensuring they stay within the pixmap bounds.
    // 5. Calculate source and destination positions for scrolling.
    // 6. Copy the relevant part of the pixmap to the window using the redraw_pixmap function.
    void scroll_window(Screen_Info& screen_info, int dx, int dy, int scroll_step) {

        int window_index = screen_info.active_window;
        Window_Info& window_info = screen_info.windows[window_index];

        // Calculate the scaling factor based on the ratio of the logical window size to the pixmap size
        double scale_x = static_cast<double>(window_info.logical_width) / static_cast<double>(window_info.pixmap_width);
        double scale_y = static_cast<double>(window_info.logical_height) / static_cast<double>(window_info.pixmap_height);

        // Update the scroll_step based on the scaling factor
        scroll_step = 1 + static_cast<int>(scroll_step * std::max(scale_x, scale_y));

        // Update the logical position with bounds checking
        int new_logical_x = window_info.logical_x + dx * scroll_step;
        int new_logical_y = window_info.logical_y + dy * scroll_step;

        // Verify the new logical x and y are within the bounds of the pixmap and set them to bounds if they exceed
        if (new_logical_x < 0)
            new_logical_x = 0;
        if (new_logical_x + window_info.logical_width > window_info.pixmap_width)
            new_logical_x = window_info.pixmap_width - window_info.logical_width;

        if (new_logical_y < 0)
            new_logical_y = 0;
        if (new_logical_y + window_info.logical_height > window_info.pixmap_height)
            new_logical_y = window_info.pixmap_height - window_info.logical_height;

        window_info.logical_x = new_logical_x;
        window_info.logical_y = new_logical_y;

        // Calculate source and destination positions for scrolling
        int src_x = window_info.logical_x;
        int src_y = window_info.logical_y;
        int dest_x = 0;
        int dest_y = 0;
        int copy_width = window_info.width;
        int copy_height = window_info.height;

        // Copy the relevant part of the pixmap to the window
        redraw_pixmap(screen_info);
    }//..
    // handle_scrolling ../
    //    -- manages arrow key events for scrolling the content of a window.
    // 1. Initialize dx and dy for scroll direction.
    // 2. Set dx or dy based on the key_sym (arrow key) and invert_scroll flag.
    // 3. Call scroll_window with the calculated dx and dy and provided scroll_step.

    void handle_scrolling(Screen_Info& screen_info, bool invert_scroll, int scroll_step, KeySym key_sym) {

        int dx = 0;
        int dy = 0;

        // Handle arrow key events for scrolling
        switch (key_sym) {
            case XK_Left: // Left arrow key
                dx = invert_scroll ? 1 : -1;
                break;
            case XK_Right: // Right arrow key
                dx = invert_scroll ? -1 : 1;
                break;
            case XK_Up: // Up arrow key
                dy = invert_scroll ? 1 : -1;
                break;
            case XK_Down: // Down arrow key
                dy = invert_scroll ? -1 : 1;
                break;
            default:
                return;
        }

        scroll_window(screen_info, dx, dy, scroll_step);
    }//..
    // zoom ../
    //    -- Adjusts the zoom level of the window based on the specified zoom_factor:
    // 1. Calculate the new logical width and height based on the desired zoom level.
    // 2. Ensure the new logical width and height are within the bounds of the pixmap.
    // 3. Calculate the new logical x and y coordinates to keep the zoom centered on the current view.
    // 4. Prevent the window from zooming outside the bounds of the pixmap.
    // 5. Update the logical x, y, width, and height of the window.
    // 6. Call the redraw_pixmap function to update the display with the new zoom level.
    void zoom(viz::Screen_Info& screen_info, double zoom_factor) {

        int window_index = screen_info.active_window;
        viz::Window_Info& window_info = screen_info.windows[window_index];

        // Calculate the new logical width and height based on the desired zoom level
        int new_logical_width = static_cast<int>(window_info.logical_width / zoom_factor);
        int new_logical_height = static_cast<int>(window_info.logical_height / zoom_factor);

        if (new_logical_width <= 0 || new_logical_height <= 0)
            return;

        if (new_logical_width > window_info.pixmap_width) {
            if (window_info.logical_width == window_info.pixmap_width || window_info.logical_height == window_info.pixmap_height)
                return;
            double ratio = static_cast<double>(window_info.pixmap_width) / static_cast<double>(new_logical_width);
            new_logical_width = window_info.pixmap_width;
            new_logical_height = static_cast<int>(new_logical_height * ratio);
        }

        if (new_logical_height > window_info.pixmap_height) {
            if (window_info.logical_width == window_info.pixmap_width || window_info.logical_height == window_info.pixmap_height)
                return;
            double ratio = static_cast<double>(window_info.pixmap_height) / static_cast<double>(new_logical_height);
            new_logical_height = window_info.pixmap_height;
            new_logical_width = static_cast<int>(new_logical_width * ratio);
        }

        // Calculate the new logical x and y coordinates to keep the zoom centered on the current view
        int new_logical_x = window_info.logical_x + (window_info.logical_width - new_logical_width) / 2;
        int new_logical_y = window_info.logical_y + (window_info.logical_height - new_logical_height) / 2;

        // Prevent the window from zooming outside the bounds of the pixmap
        if (new_logical_x < 0)
            new_logical_x = 0;
        if (new_logical_y < 0)
            new_logical_y = 0;
        if (new_logical_x + new_logical_width > window_info.pixmap_width)
            new_logical_x = window_info.pixmap_width - new_logical_width;
        if (new_logical_y + new_logical_height > window_info.pixmap_height)
            new_logical_y = window_info.pixmap_height - new_logical_height;

        // Update the logical x, y, width, and height of the window
        window_info.logical_x = new_logical_x;
        window_info.logical_y = new_logical_y;
        window_info.logical_width = new_logical_width;
        window_info.logical_height = new_logical_height;

        // Call the redraw_pixmap function to update the display with the new zoom level
        redraw_pixmap(screen_info);
    }//..
    // handle_zoom ../
    void handle_zoom(Screen_Info& active_screen_info, double zoom_out_step, double zoom_in_step, KeySym key_sym, bool is_shift_pressed)
    {
        switch (key_sym) {
            case XK_minus:
                viz::zoom(active_screen_info, zoom_out_step);
                break;
            case XK_equal:
                if (is_shift_pressed) {
                    viz::zoom(active_screen_info, zoom_in_step);
                }
                break;
        }
    }//..

}//..

// Utility function to rotate a point around another point
void rotate_point(double cx, double cy, double angle, double& x, double& y) {
    double s = sin(angle);
    double c = cos(angle);

    // Translate point back to origin
    x -= cx;
    y -= cy;

    // Rotate point
    double x_new = x * c - y * s;
    double y_new = x * s + y * c;

    // Translate point back
    x = x_new + cx;
    y = y_new + cy;
}


int main() { // Demo of viz module
    // Initialize random number generator
    std::srand(std::time(nullptr));
    // config ../
    //
    viz::Window_Config config = {
        // logical_screens: x, y, width, height
        {
            std::make_tuple(0, 0, 1440, 720) // SCREEN3
        },
        // window_positions_per_screen: x, y, width, height
        {
            { // SCREEN3
                std::make_tuple(0, 0, 700, 720), // WINDOW3A
                std::make_tuple(700, 0, 40, 720), // WINDOW3A
                std::make_tuple(740, 0, 700, 720) // WINDOW3B
            }
        },
        // pixmap_dimensions_per_screen: width, height
        {
            {
                std::make_tuple(1800, 5600), // WINIDOW3!
                std::make_tuple(1800, 5600), // WINIDOW3!
                std::make_tuple(1800, 5600) // WINIDOW3!
            }

        },
        {0}, // Active window for each screen
        0   // ACtive screen
    };//..
    std::vector<viz::Screen_Info> screen_info_list = viz::create_windows(config);

    // Draw random rectangles or circles on the pixmaps of all windows
    bool draw_rectangle = true;
    for (auto& screen_info : screen_info_list) {
        for (auto& window_info : screen_info.windows) {
            // Set the background color to black
            XSetForeground(screen_info.display, window_info.gc, BlackPixel(screen_info.display, screen_info.screen_number));
            XFillRectangle(screen_info.display, window_info.pixmap, window_info.gc, 0, 0, window_info.pixmap_width, window_info.pixmap_height);

            // Draw random rectangles or circles
            for (int i = 0; i < 100; ++i) {
                int x = std::rand() % window_info.pixmap_width;
                int y = std::rand() % window_info.pixmap_height;

                // Generate green shades only
                int red = std::rand() % 25;
                int green = std::rand() % 256;
                int blue = red;
                unsigned long color = (red << 16) | (green << 8) | blue;

                XSetForeground(screen_info.display, window_info.gc, color);

                if (draw_rectangle) {
                    int width = std::rand() % 150;
                    int height = std::rand() % 150;
                    XFillRectangle(screen_info.display, window_info.pixmap, window_info.gc, x, y, width, height);
                } else {
                    int radius = std::rand() % 55;
                    int diameter = 2 * radius;
                    XFillArc(screen_info.display, window_info.pixmap, window_info.gc, x, y, diameter, diameter, 0, 360 * 64);
                }
            }

            // Initial redraw of the pixmap
            viz::draw_pixmap(screen_info, &window_info - &screen_info.windows.front());

            // Toggle the draw_rectangle flag for the next window
            draw_rectangle = !draw_rectangle;
        }
    }
    // Event loop
    XEvent event;
    int revert_to;
    Display * display = screen_info_list[0].display; // all screens share the same display
    Window active_screen = event.xfocus.window;
    auto active_screen_it = std::find_if(screen_info_list.begin(), screen_info_list.end(),
            [&active_screen](const viz::Screen_Info& info) {
            return info.logical_screen == active_screen;
            });

    while (true)
    {// Find the display with the next event
        XNextEvent(display, &event);
        if (event.type == FocusIn) {
            active_screen = event.xfocus.window;
            // Find the Screen_Info containing the active logical screen
            active_screen_it = std::find_if(screen_info_list.begin(), screen_info_list.end(),
                    [&active_screen](const viz::Screen_Info& info) {
                    return info.logical_screen == active_screen;
                    });
        }
        viz::Screen_Info& active_screen_info = *active_screen_it;

        // Check for events within the active logical screen

        // Handle window close events
        if (event.type == ClientMessage) {
            // Clean up resources for the closed window
            for (const auto& window_info : active_screen_info.windows) {
                XFreeGC(display, window_info.gc);
            }
            // Remove the screen from the screen_info_list
            screen_info_list.erase(active_screen_it);
            if (screen_info_list.empty()) {
                break;
            }
            continue;
        }

        if (event.type == KeyPress) {
            KeySym key_sym = XLookupKeysym(&event.xkey, 0);

            // Handle scrolling events
            viz::handle_scrolling(active_screen_info, false, 150, key_sym);
            viz::handle_zoom(active_screen_info, 0.5, 2, key_sym, event.xkey.state & ShiftMask);
            switch (key_sym) {
                case 0x0078: // x to break for debug
                    break;
            }
        }
    //    if (event.type == ConfigureNotify) {
     //       handle_resize(active_screen_info, event.xconfigure);
      //  }
        // Redraw the pixmap if needed
        if (event.type == Expose)
            viz::redraw_pixmap(active_screen_info);
    }


    // Clean up resources
    for (const auto& screen_info : screen_info_list) {
        for (const auto& window_info : screen_info.windows) {
            XFreeGC(screen_info.display, window_info.gc);
        }
    }

    // Close the X11 display
    XCloseDisplay(screen_info_list[0].display);

    return 0;
}
