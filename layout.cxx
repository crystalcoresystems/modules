#include <iostream>
#include <vector>
#include <string>
#include <unordered_set>
#include <utility>
#include <climits>
#include <algorithm>
#include <tuple>
void bail(std::string msg) {
    std::cerr << msg << std::endl;
     exit(1);
}
// structs../
// Layout:
// This struct represents a user-defined layout, containing a scaling factor and a matrix of characters.
struct Layout {
    int                      scale;  // Scaling factor for the layout
    std::vector<std::string> layout; // Matrix of characters representing the layout
};

// pair_hash:
// This struct provides a hash function for pairs, which can be used in unordered containers.
struct pair_hash {
    template <class T1, class T2>
    std::size_t operator() (const std::pair<T1, T2>& p) const {
        auto h1 = std::hash<T1>{}(p.first); // Compute the hash for the first element
        auto h2 = std::hash<T2>{}(p.second); // Compute the hash for the second element

        // Combine the two hashes into a single value
        return h1 ^ (h2 << 1);
    }
};

// DFS_Bounds:
// This struct holds the minimum and maximum row and column indices for an object in the layout matrix.
struct DFS_Bounds {
    int min_row; // Minimum row index
    int max_row; // Maximum row index
    int min_col; // Minimum column index
    int max_col; // Maximum column index
};//..
// dfs_bounds../
// This function performs a depth-first search (DFS) on the layout matrix to find the minimum and maximum
// row and column indices of the object represented by character 'c'.
// It takes the layout matrix, visited matrix, current row and column indices, a DFS_Bounds struct to store
// the bounds, and the character 'c' that represents the object.
void dfs_bounds(
        const std::vector<std::string>& layout,    // The layout matrix
        std::vector<std::vector<bool>>& visited,   // The visited matrix to keep track of traversed cells
        int                             row,       // Current row index
        int                             col,       // Current column index
        DFS_Bounds&                     bounds,     // Bounds structure to store min/max row/column indices
        char                            c          // Character representing the object
    ) {

    if (row < 0 || col < 0 || row >= layout.size() || col >= layout[0].size() || visited[row][col] || layout[row][col] != c) {
        return;
    }

    visited[row][col] = true;

    bounds.min_row = std::min(bounds.min_row, row);
    bounds.max_row = std::max(bounds.max_row, row);
    bounds.min_col = std::min(bounds.min_col, col);
    bounds.max_col = std::max(bounds.max_col, col);

    dfs_bounds(layout, visited, row - 1, col, bounds,c);
    dfs_bounds(layout, visited, row + 1, col, bounds,c);
    dfs_bounds(layout, visited, row, col - 1, bounds,c);
    dfs_bounds(layout, visited, row, col + 1, bounds,c);
}//..
// count_boundary_characters../
// This function calculates the number of boundary characters for an object in the layout matrix.
// It takes the object character 'c', layout matrix, DFS_Bounds struct, and four integer references for
// leftmost, rightmost, topmost, and bottommost counts.
void count_boundary_characters(
        char                            c,         // Character representing the object
        const std::vector<std::string>& layout,    // The layout matrix
        const DFS_Bounds&               bounds,    // Bounds structure with min/max row/column indices
        int&                            leftmost_count,   // Count of leftmost boundary characters
        int&                            rightmost_count,  // Count of rightmost boundary characters
        int&                            topmost_count,    // Count of topmost boundary characters
        int&                            bottommost_count  // Count of bottommost boundary characters
    ) {

    leftmost_count = 0;
    rightmost_count = 0;
    topmost_count = 0;
    bottommost_count = 0;

    for (int row = bounds.min_row; row <= bounds.max_row; ++row) {
        for (int col = bounds.min_col; col <= bounds.max_col; ++col) {
            if (layout[row][col] == c) {
                if (row == bounds.min_row) {
                    ++topmost_count;
                }
                if (row == bounds.max_row) {
                    ++bottommost_count;
                }
                if (col == bounds.min_col) {
                    ++leftmost_count;
                }
                if (col == bounds.max_col) {
                    ++rightmost_count;
                }
            }
        }
    }
}//..
// validate_and_normalize_layout../
// This function takes a layout structure as input and validates it to ensure the layout is valid.
// It checks for the presence of rectangular objects with unique character definitions and no overlapping objects.
// If the layout is valid, it normalizes the bounds of each object by multiplying them by the scale factor provided in the layout_struct.
// The function returns a vector of tuple<int, int, int, int> objects, which represent the normalized bounds of each object in the layout.

std::vector<std::tuple<int, int, int, int>>
validate_and_normalize_layout(
        std::vector<std::string> layout,
        int                      pixels_per_character
    ) {

    int rows = layout.size();
    int cols = layout[0].size();

    std::vector<std::vector<bool>> visited(rows, std::vector<bool>(cols, false));
    std::vector<char> object_list;
    std::vector<std::tuple<int, int, int, int>> normalized_bounds;

    DFS_Bounds bounds;
    int leftmost_count, rightmost_count, topmost_count, bottommost_count;

    // Find the bounds of the object using DFS
    for (int row = 0; row < rows; row++) {
        for (int col = 0; col < cols; col++) {
            if (!visited[row][col] && layout[row][col] != '.') {
                if (std::find(object_list.begin(), object_list.end(), layout[row][col]) != object_list.end()) {
                    // There's more than one object with the same character definition
                    bail("Invalid Layout!\n");
                }

                object_list.push_back(layout[row][col]);
                bounds.max_col = col;
                bounds.min_col = col;
                bounds.max_row = row;
                bounds.min_row = row;

                dfs_bounds(layout, visited, row, col, bounds, layout[row][col]);
                // Check if the object has a rectangular shape
                count_boundary_characters(layout[row][col], layout, bounds, leftmost_count, rightmost_count, topmost_count, bottommost_count);
                if ((leftmost_count != rightmost_count) || (topmost_count != bottommost_count)) {
                    bail("Invalid Layout!\n");
                }

                // Normalize the bounds by multiplying them by the scale factor
                int min_x = bounds.min_col * pixels_per_character;
                int min_y = bounds.min_row * pixels_per_character;
                int width = (bounds.max_col - bounds.min_col + 1) * pixels_per_character;
                int height = (bounds.max_row - bounds.min_row + 1) * pixels_per_character;

                normalized_bounds.emplace_back(min_x, min_y, width, height);
            }
        }
    }

    if (object_list.empty()) {
        // There's no object in the layout
        bail("Invalid Layout!\n");
    }
    return normalized_bounds;
}//..

int main() {
    // User-defined layouts
    Layout layout1 = {
        10, // scale
        {
            "AAAA",
            "AAAA",
            "BBBB"
        }
    };

    Layout layout2 = {
        20, // scale
        {
            "AA",
            "AA",
            "BB"
        }
    };

    // Add the layouts to a vector
    std::vector<Layout> layouts = {layout1, layout2};

    std::vector<std::tuple<int, int, int, int>> normalized_bounds;


    // Validate and normalize user layouts
    for (auto& screen : layouts) {
        normalized_bounds = validate_and_normalize_layout(screen.layout, screen.scale);

        // Print the normalized bounds
        std::cout << "Normalized Bounds:" << std::endl;
        for (const auto& bound : normalized_bounds) {
            std::cout << "(" << std::get<0>(bound) << ", " << std::get<1>(bound) << ", " << std::get<2>(bound) << ", " << std::get<3>(bound) << ")" << std::endl;
        }
    }

    return 0;
}

